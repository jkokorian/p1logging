import serial
from collections import namedtuple
import click
from datetime import datetime
import json

def create_serial(port_name):
    #Set COM port config
    s = serial.Serial()
    s.baudrate = 115200
    s.bytesize=serial.EIGHTBITS
    s.parity=serial.PARITY_NONE
    s.stopbits=serial.STOPBITS_ONE
    s.xonxoff=1
    s.rtscts=0
    s.timeout=20
    s.port=port_name
    s.open()
    return s
    
MessageDefinition = namedtuple("MessageDefinition", ["OBIS_reference", "key", "unit", "parse_value"])

messages = [
    MessageDefinition("1-0:1.8.1", "stroom_afname_dal_cumulatief", "kWh", lambda line: float(line[10:15])),
    MessageDefinition("1-0:1.8.2", "stroom_afname_piek_cumulatief", "kWh", lambda line: float(line[10:15])),
    MessageDefinition("1-0:2.8.1", "stroom_levering_dal_cumulatief",  "kWh",lambda line: float(line[10:15])),
    MessageDefinition("1-0:2.8.2", "stroom_levering_piek_cumulatief", "kWh", lambda line: float(line[10:15])),
    MessageDefinition("1-0:1.7.0", "stroom_afname_actueel", "W", lambda line: float(line[10:-4]) * 1000),
    MessageDefinition("1-0:2.7.0", "stroom_levering_actueel", "W", lambda line: float(line[10:-4]) * 1000),
    MessageDefinition("0-1:24.2.1", "gas_afname_cumulatief", "m3", lambda line: float(line[-13:-4])),
    MessageDefinition("0-0:1.0.0", "timestamp", "YYMMDDhhmmss", lambda line: datetime.strptime(line[10:-2], r"%y%m%d%H%M%S")),
]

def parse(line):
    for message in messages:
        if line.startswith(message.OBIS_reference):
            return {message.key: message.parse_value(line)}
    return {}


def fetch_telegrams(serial_handle):
    """ Reads the serial port until we can create a reading point. """
    
    telegram_start_seen = False
    buffer = ''

    # Just keep fetching data until we got what we were looking for.
    for data in serial_handle:
        try:
            data = str(data, 'utf-8')
        except TypeError:
            pass

        if data.startswith('/'):
            telegram_start_seen = True
            buffer = ''

        if telegram_start_seen:
            buffer += data

        # Telegrams ends with '!' AND we saw the start. We should have a complete telegram now.
        if data.startswith('!') and telegram_start_seen:
            yield buffer

            # Reset the flow again.
            telegram_start_seen = False
            buffer = ''

def parse_telegram(telegram):
    parsed_lines = [parse(line) for line in telegram.splitlines()]
    return {k:v for parsed in parsed_lines for k, v in parsed.items()}


def read_p1(serial_port):
    for telegram in fetch_telegrams(serial_port):
        data = parse_telegram(telegram)
        data['timestamp'] = datetime.now()
        yield data

@click.command()
@click.argument('serial_port', type=str)
@click.option('--outfile', default='-', type=click.File(mode='w'))
def cli(serial_port, outfile):
    s = create_serial(serial_port)
    for data in read_p1(serial_port):
        outfile.write(json.dumps(data, default=str))

if __name__ == "__main__":
    cli()