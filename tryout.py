"""
    NOTE: This script asumes:

    - Smart meter supporting DSMR v4+ (see settings around line 40).
    - The default serial port for the cable at ttyUSB0 (also see settings around line 40).

"""
from time import sleep

from serial.serialutil import SerialException
import requests
import serial


def main():
    print('Starting...')
    handle = create_serial()
    for telegram in read_telegram(handle):
        print('Telegram read')
        print(telegram)

        sleep(1)


def create_serial():
    serial_handle = serial.Serial()
    serial_handle.port = 'COM3'
    serial_handle.baudrate = 115200
    serial_handle.bytesize = serial.EIGHTBITS
    serial_handle.parity = serial.PARITY_NONE
    serial_handle.stopbits = serial.STOPBITS_ONE
    serial_handle.xonxoff = 1
    serial_handle.rtscts = 0
    serial_handle.timeout = 20

    # This might fail, but nothing we can do so just let it crash.
    serial_handle.open()
    return serial_handle

def read_telegram(serial_handle):
    """ Reads the serial port until we can create a reading point. """
    
    telegram_start_seen = False
    buffer = ''

    # Just keep fetching data until we got what we were looking for.
    try:
        for data in serial_handle:
            try:
                # Make sure weird characters are converted properly.
                data = str(data, 'utf-8')
            except TypeError:
                pass

            if data.startswith('/'):
                telegram_start_seen = True
                buffer = ''

            if telegram_start_seen:
                buffer += data

            # Telegrams ends with '!' AND we saw the start. We should have a complete telegram now.
            if data.startswith('!') and telegram_start_seen:
                yield buffer

                # Reset the flow again.
                telegram_start_seen = False
                buffer = ''


    except SerialException as error:
        # Something else and unexpected failed.
        print('Serial connection failed:', error)
        return  # Break out of yield.


if __name__ == '__main__':
    main()