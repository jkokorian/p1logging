from p1logger import parse, parse_telegram, fetch_telegrams
from datetime import datetime

def test_parse_line_stroom_afname_dal_cumulatief():
    parsed = parse("1-0:1.8.1(00068.000*kWh)")
    assert "stroom_afname_dal_cumulatief" in parsed
    assert parsed['stroom_afname_dal_cumulatief'] == 68.000
    

def test_parse_line_stroom_afname_piek_cumulatief():
    parsed = parse("1-0:1.8.2(00024.000*kWh)")
    assert "stroom_afname_piek_cumulatief" in parsed

def test_parse_stroom_levering_dal_cumulatief():
    assert "stroom_levering_dal_cumulatief" in parse("1-0:2.8.1(00027.000*kWh)")

def test_parse_stroom_levering_piek_cumulatief():
    assert "stroom_levering_piek_cumulatief" in parse("1-0:2.8.2(00028.000*kWh)")

def test_parse_stroom_afname_actueel():
    assert "stroom_afname_actueel" in parse("1-0:1.7.0(0000.00*kW)")

def test_parse_stroom_levering_actueel():
    assert "stroom_levering_actueel" in parse("1-0:2.7.0(0000.00*kW)")

def test_parse_gas_afname_cumulatief():
    assert "gas_afname_cumulatief" in parse("0-1:24.2.1(181223220000W)(02328.533*m3)")


def test_parse_timestamp():
    parsed = parse("0-0:1.0.0(181223225522W)")
    assert "timestamp" in parsed
    assert parsed['timestamp'] == datetime(year=2018, month=12, day=23, hour=22, minute=55, second=22)

def test_parse_telegram_complete_telegram():
    telegram = """/XMX5LGBBFG1012773335

1-3:0.2.8(42)
0-0:1.0.0(181223225522W)
0-0:96.1.1(4530303331303033373237303938353137)
1-0:1.8.1(001274.643*kWh)
1-0:1.8.2(001184.728*kWh)
1-0:2.8.1(000001.344*kWh)
1-0:2.8.2(000000.000*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(00.213*kW)
1-0:2.7.0(00.000*kW)
0-0:96.7.21(00004)
0-0:96.7.9(00001)
1-0:99.97.0(1)(0-0:96.7.19)(180622141406S)(0000002697*s)
1-0:32.32.0(00000)
1-0:32.36.0(00000)
0-0:96.13.1()
0-0:96.13.0()
1-0:31.7.0(001*A)
1-0:21.7.0(00.213*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303235303033353036303635313137)
0-1:24.2.1(181223220000W)(02328.533*m3)
!D4C0"""

    parsed = list(parse_telegram(telegram))
    assert any(parsed)

def test_fetch_telegrams_actual_telegram():
    telegram = """/XMX5LGBBFG1012773335

1-3:0.2.8(42)
0-0:1.0.0(181223225522W)
0-0:96.1.1(4530303331303033373237303938353137)
1-0:1.8.1(001274.643*kWh)
1-0:1.8.2(001184.728*kWh)
1-0:2.8.1(000001.344*kWh)
1-0:2.8.2(000000.000*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(00.213*kW)
1-0:2.7.0(00.000*kW)
0-0:96.7.21(00004)
0-0:96.7.9(00001)
1-0:99.97.0(1)(0-0:96.7.19)(180622141406S)(0000002697*s)
1-0:32.32.0(00000)
1-0:32.36.0(00000)
0-0:96.13.1()
0-0:96.13.0()
1-0:31.7.0(001*A)
1-0:21.7.0(00.213*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303235303033353036303635313137)
0-1:24.2.1(181223220000W)(02328.533*m3)
!D4C0"""

    serial_data = (line + "\n" for  line in telegram.splitlines()[7:] + telegram.splitlines())
    assert telegram == next(fetch_telegrams(serial_data)).strip()

def test_fetch_telegrams_from_slash_to_exclamation():
    serial_data = (c + "\n" for c in "notinteresting/actual telegram!notinteresting")

    assert next(fetch_telegrams(serial_data)).replace("\n", "") == "/actual telegram!"

